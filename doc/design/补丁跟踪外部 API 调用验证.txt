

1. 列出组织下的仓库
curl -X GET --header 'Content-Type: application/json;charset=UTF-8' 'https://gitee.com/api/v5/orgs/src-openEuler/repos?access_token=6c9db45c752e3697e860cd60fc3dfd8e&type=all&page=1&per_page=1'


2. 列出仓库下的分支
curl -X GET --header 'Content-Type: application/json;charset=UTF-8' 'https://gitee.com/api/v5/repos/src-openeuler/openEuler-repos/branches?access_token=6c9db45c752e3697e860cd60fc3dfd8e'


3. 访问分支下 xxx.yaml
curl -X GET --header 'Content-Type: application/json;charset=UTF-8' 'https://gitee.com/api/v5/repos/src-openeuler/rust/contents/rust.yaml?access_token=6c9db45c752e3697e860cd60fc3dfd8e' | python -m json.tool | grep '"content"' | awk '{print $2}' | tr -d '",' | base64.exe -d


4. 对比 Commit 并创建补丁文件
scm_commit=54bc3e5566
last_commit=$( curl https://api.github.com/repos/rust-lang/rust/branches/stable | grep -A 1 "commit" | grep -w sha | grep -oE '[^"]{40}' | grep -oE "^\w{10}" )
echo ${last_commit}
curl https://github.com/rust-lang/rust/compare/${scm_commit}...${last_commit}.diff >${scm_commit}...${last_commit}.patch


5. 创建临时分支 patch-tracking
curl -X POST --header 'Content-Type: application/json;charset=UTF-8' 'https://gitee.com/api/v5/repos/chenyanpanHW/rust/branches' -d '{"access_token":"4743632949a5d8f2ef293f54e0ba1a81","refs":"openEuler-20.03-LTS","branch_name":"patch-tracking"}'


6. 在 patch-tracking 提交补丁文件
curl -X POST --header 'Content-Type: application/json;charset=UTF-8' 'https://gitee.com/api/v5/repos/chenyanpanHW/rust/contents/1.patch' -d '{"access_token":"4743632949a5d8f2ef293f54e0ba1a81","content":"ZGlmZiAtLWdpdCBhL3NyYy9saWJjb3JlL2ludGVybmFsX21hY3Jvcy5ycyBiL3NyYy9saWJjb3JlL2ludGVybmFsX21hY3Jvcy5ycwppbmRleCA1ZjU3OGU0NDMyM2VjLi44MzA0YTY3NjkxODUzIDEwMDY0NAotLS0gYS9zcmMvbGliY29yZS9pbnRlcm5hbF9tYWNyb3MucnMKKysrIGIvc3JjL2xpYmNvcmUvaW50ZXJuYWxfbWFjcm9zLnJzCkBAIC0xMTgsNiArMTE4LDcgQEAgbWFjcm9fcnVsZXMhIGltcGxfZm5fZm9yX3pzdCB7CiAgICAgfQogfQogCisjW2FsbG93KHVudXNlZF9tYWNyb3MpXQogbWFjcm9fcnVsZXMhIGxsdm1fYXNtIHsKICAgICAvLyBSZWRpcmVjdCB0byBhc20hIGZvciBzdGRhcmNoIGluIHN0YWJsZSAxLjQzCiAgICAgKCQoJGFyZzp0dCkqKSA9PiB7ICRjcmF0ZTo6YXNtISgkKCRhcmcpKikgfQo=","message":"patch tracking","branch":"patch-tracking"}'


7. 创建 issue
issue_url=$( curl -X POST --header 'Content-Type: application/json;charset=UTF-8' 'https://gitee.com/api/v5/repos/chenyanpanHW/issues' -d '{"access_token":"4743632949a5d8f2ef293f54e0ba1a81","repo":"rust","title":"patch tracking","body":"patch file from commit xxx to xxx"}' | grep -oE '"html_url":"[^"]+"' | grep -oE '[^"]+/issues/[^"]+' )
issue=${issue_url##*/}


8. 创建 PR
curl -X POST --header 'Content-Type: application/json;charset=UTF-8' 'https://gitee.com/api/v5/repos/chenyanpanHW/rust/pulls' -d "{\"access_token\":\"4743632949a5d8f2ef293f54e0ba1a81\",\"title\":\"patch-tracking\",\"head\":\"patch-tracking\",\"base\":\"openEuler-20.03-LTS\",\"issue\":\"${issue}\"}"


