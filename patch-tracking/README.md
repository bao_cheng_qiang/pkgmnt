Patch Tracking
=============

## 1 先决条件

环境已安装 Python >= 3.7 以及 pip3

## 2 安装

#### 2.1 安装依赖包

```shell script
pip3 install flask flask-restx flask-sqlalchemy flask-apscheduler requests
```

#### 2.2 安装补丁跟踪工具 rpm 包

```shell script
rpm -ivh patch-tracking-xxx.rpm
```


## 3 配置

配置文件路径 `/etc/patch-tracking/settings.conf`


- 服务监听地址

```python
FLASK_SERVER_NAME = '127.0.0.1:5001'
```

- GitHub Token，用于访问托管在 GitHub 上游开源软件仓的仓库信息

```python
GITHUB_ACCESS_TOKEN = '<GitHub token>'
```

- 对于托管在gitee上的需要跟踪的仓库，配置一个有该仓库权限的gitee的token，用于提交patch文件，提交issue，提交PR等操作。

```python
GITEE_ACCESS_TOKEN = '<Gitee token>'
GITEE_OWNER = '<Gitee Owner>'
```

- 定时扫描数据库中是否有新增或修改的跟踪项，在这里配置扫描的时间间隔，数字单位是秒
    
```python
SCAN_DB_INTERVAL = 3600
```
    
- 定时执行去上游仓库查看是否有更新，对该仓库进行补丁跟踪提交，在这里配置查询上游仓库的时间间隔，数字单位是秒。

```python
TRACKING_JOB_INTERVAL = 3600
```

## 4 运行

#### 4.1 启动服务(后续会修改为 systemd 启动方式)

```shell script
/usr/bin/patch-tracking
```

#### 4.2 执行命令工具

> 命令行用法见下文

```shell script
/usr/bin/patch-tracking-cli --repo docker --branch master --scm_repo custa/docker-ce --scm_branch master --enabled True
```

#### 4.3 访问 RESTful 接口查询补丁跟踪信息

- 查询 tracking

```shell script
curl http://127.0.0.1:5001/tracking
```

- 查询 issue

```shell script
curl http://127.0.0.1:5001/issue
```

## 5 命令行工具使用说明

将需要跟踪的软件仓库和分支与其上游开源软件仓库与分支关联起来，有 3 种使用方法

- 命令行直接添加

```shell script
patch-tracking-cli --server 127.0.0.1:5001 --version_control github --repo testPatch1 --branch master --scm_repo BJMX/testPatch01 --scm_branch test  --enable true
```

> --server ：启动Patch Tracking服务的URL，例如：127.0.0.1:5001 \
--version_control :上游仓库版本的控制工具，只支持github, gitlab, svn \
--repo 需要进行跟踪的仓库名称 \
--branch 需要进行跟踪的仓库的分支名称 \
--scm_repo 被跟踪的上游仓库的仓库名称，github格式：组织/仓库 \
--scm_branch 被跟踪的上游仓库的仓库的分支 \
--enable 是否自动跟踪该仓库


- 指定文件添加

文件内容就是命令行参数的那些必填项，将这些写入文件名为xxx.yaml，例如tracking.yaml：

```shell script
patch-tracking-cli --server 127.0.0.1:5001 --file tracking.yaml
```

> --server ：启动Patch Tracking服务的URL，例如：127.0.0.1:5001 \
--file ：yaml文件路径

```shell script
version_control: github
scm_repo: xxx/xxx
scm_branch: master
repo: xxx
branch: master
enabled: true
```


- 指定目录添加

在指定的目录，例如test_yaml下放入多个xxx.yaml文件，执行命令，记录所有yaml文件的跟踪项

```shell script
patch-tracking-cli --server 127.0.0.1:5001 --dir /home/Work/test_yaml/
```

> --server ：启动Patch Tracking服务的URL，例如：127.0.0.1:5001 \
--dir ：存放yaml文件目录的路径

