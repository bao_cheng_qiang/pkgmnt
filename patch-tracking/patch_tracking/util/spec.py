import re


class Spec:
    def __init__(self, content):
        self._content = content
        self._lines = content.splitlines()
        self.version = "0.0"
        self.release = 0
        self.release_lineno = 0
        self.source_lineno = 0
        self.patch_lineno = 0
        self.patch_threshold = 4000
        self.max_patch_num = 0
        self.changelog_lineno = 0

        for i, line in enumerate(self._lines):
            match_find = re.match(r"[ \t]*Version:[ \t]*([\d.]+)", line)
            if match_find:
                self.version = match_find[1]
                continue

            match_find = re.match(r"[ \t]*Release:[ \t]*([\d.]+)", line)
            if match_find:
                self.release = int(match_find[1])
                self.release_lineno = i
                continue

            match_find = re.match(r"[ \t]*%changelog", line)
            if match_find:
                self.changelog_lineno = i
                continue

            match_find = re.match(r"[ \t]*Source([\d]*):", line)
            if match_find:
                self.source_lineno = i
                continue

            match_find = re.match(r"[ \t]*Patch([\d]+):", line)
            if match_find:
                num = int(match_find[1])
                self.patch_lineno = 0
                if num > self.max_patch_num:
                    self.max_patch_num = num
                self.patch_lineno = i
                continue

        if self.patch_lineno == 0:
            self.patch_lineno = self.source_lineno

        if self.max_patch_num < self.patch_threshold:
            self.max_patch_num = self.patch_threshold

    def update(self, log_title, log_content, patches):
        """
            Update items in spec file
        """
        self.release += 1
        self._lines[self.release_lineno] = re.sub(r"[\d]+", str(self.release), self._lines[self.release_lineno])

        log_title = "* " + log_title + " " + self.version + "-" + str(self.release)
        log_content = "- " + log_content
        self._lines.insert(self.changelog_lineno + 1, log_title + "\n" + log_content + "\n")

        patch_list = []
        for patch in patches:
            patch_list.append("Patch" + str(self.max_patch_num) + ": " + patch)
            self.max_patch_num += 1
        self._lines.insert(self.patch_lineno + 1, "\n".join(patch_list))

        return self.__str__()

    def __str__(self):
        return "\n".join(self._lines)


if __name__ == "__main__":
    SPEC_CONTENT = """Name: diffutils
Version: 3.7
Release: 3

Source: ftp://ftp.gnu.org/gnu/diffutils/diffutils-%{version}.tar.xz

Patch1: diffutils-cmp-s-empty.patch
Patch2: diffutils-i18n.patch

%changelog
* Mon Nov 11 2019 shenyangyang<shenyangyang4@huawei.com> 3.7-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded comments

* Thu Oct 24 2019 shenyangyang<shenyangyang4@huawei.com> 3.7-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add build requires of gettext-devel

* Thu Aug 29 2019 hexiaowen <hexiaowen@huawei.com> - 3.7-1
- Package init
"""

    s = Spec(SPEC_CONTENT)
    s.update("Mon Nov 11 2019 patch-tracking", "DESC:add patch files", [
        "xxx.patch",
        "yyy.patch",
    ])

    print(s)
