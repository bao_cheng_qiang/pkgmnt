import time
import logging
import requests
from patch_tracking import settings

logger = logging.getLogger(__name__)


class GitHubApi:
    def __init__(self):
        token = 'token ' + settings.GITHUB_ACCESS_TOKEN
        self.headers = {
            'User-Agent': 'Mozilla/5.0',
            'Authorization': token,
            'Content-Type': 'application/json',
            'Connection': 'close',
            'method': 'GET',
            'Accept': 'application/json'
        }

    def api_request(self, url):
        logger.debug("Connect url: %s", url)
        count = 30
        while count > 0:
            try:
                response = requests.get(url, headers=self.headers)
                return response
            # pylint: disable=W0703
            except Exception as err:
                logger.warning(err)
                logger.warning('Connect url: %s failed, sleep 2s...', url)
                time.sleep(2)
                count -= 1
                continue
        if count == 0:
            logger.error('Fail to connnect to github: %s after retry 30 times.', url)
            return 'connect error'

    def get_commit_info(self, repo_url, commit_id):
        res_dict = dict()
        api_url = 'https://api.github.com/repos'
        url = '/'.join([api_url, repo_url, 'commits', commit_id])
        ret = self.api_request(url)
        if ret != 'connect error':
            if ret.status_code == 200:
                res_dict['commit_id'] = commit_id
                res_dict['message'] = ret.json()['commit']['message']
                res_dict['time'] = ret.json()['commit']['author']['date']
                if 'parents' in ret.json() and ret.json()['parents']:
                    res_dict['parent'] = ret.json()['parents'][0]['sha']
                return 'success', res_dict

            logger.error('%s failed. Return val: %s', url, ret)
            return 'error', ret.json()
        return 'error', 'connect error'

    def get_latest_commit(self, repo_url, branch):
        '''
        根据仓库名和分支获取最新commitID
        :param repo_url:
        :param branch:
        :return:
        '''
        api_url = 'https://api.github.com/repos'
        url = '/'.join([api_url, repo_url, 'branches', branch])
        ret = self.api_request(url)
        res_dict = dict()
        if ret != 'connect error':
            if ret.status_code == 200:
                res_dict['latest_commit'] = ret.json()['commit']['sha']
                res_dict['message'] = ret.json()['commit']['commit']['message']
                res_dict['time'] = ret.json()['commit']['commit']['committer']['date']
                return 'success', res_dict

            logger.error('%s failed. Return val: %s', url, ret)
            return 'error', ret.json()

        return 'error', 'connect error'

    def get_patch(self, repo_url, scm_commit, last_commit):
        '''
        获取原commit和新commit之间的差异，在指定路径下创建patch文件
        :param repo_url:
        :param scm_commit:
        :param last_commit:
        :return:
        '''
        api_url = 'https://github.com'
        if scm_commit != last_commit:
            commit = scm_commit + '...' + last_commit + '.diff'
        else:
            commit = scm_commit + '^...' + scm_commit + '.diff'
        ret_dict = dict()

        url = '/'.join([api_url, repo_url, 'compare', commit])
        ret = self.api_request(url)
        if ret != 'connect error':
            if ret.status_code == 200:
                patch_content = ret.text
                ret_dict['status'] = 'success'
                ret_dict['api_ret'] = patch_content
            else:
                logger.error('%s failed. Return val: %s', url, ret)
                ret_dict['status'] = 'error'
                ret_dict['api_ret'] = ret.text
        else:
            ret_dict['status'] = 'error'
            ret_dict['api_ret'] = 'fail to connect github by api.'

        return ret_dict
