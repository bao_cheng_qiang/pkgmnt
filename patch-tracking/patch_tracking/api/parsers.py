from flask_restx import reqparse

pagination_arguments = reqparse.RequestParser()
pagination_arguments.add_argument('page', type=int, required=False, default=1, help='Page number')
pagination_arguments.add_argument('bool', type=bool, required=False, default=1, help='Page number')
pagination_arguments.add_argument(
    'per_page',
    type=int,
    required=False,
    choices=[2, 10, 20, 30, 40, 50],
    default=10,
    help='Results per page {error_msg}'
)

tracking_request = reqparse.RequestParser()
tracking_request.add_argument('repo', type=str, required=False, default="", help='Repository')
tracking_request.add_argument('branch', type=str, required=False, default="", help='Branch')
tracking_request.add_argument('enabled', type=bool, required=False, help='Enabled patch tracking')

issue_request = reqparse.RequestParser()
issue_request.add_argument('repo', type=str, required=False, default="", help='Repository')
issue_request.add_argument('branch', type=str, required=False, default="", help='Branch')
