from . import mysql_config
import pymysql


class MySqlHelper ():
    '''
    功能描述：对pymql的简单封装
    接口：
    修改记录：
    '''

    def __init__(self, host=None, user=None, password=None, port=None, db_name=None, charset='utf-8', *args, **Kwargs):
        self.host = host
        if self.host is None:
            self.host = mysql_config.HOST
        self.user = user
        if self.user is None:
            self.user = mysql_config.USER

        self.password = password
        if self.password is None:
            self.password = mysql_config.PASSWORD

        self.port = port
        if self.port is None:
            self.port = mysql_config.PORT

        self.db_name = db_name
        if self.db_name is None:
            self.db_name.mysql_config.DBNAME
        self.charset = charset
        self.conn = None
        self.cursor = None

    def __enter__(self):
        '''
        功能描述：连接mysql数据库
        参数：
        返回值：
        异常描述：
        修改记录：
        '''
        self.conn = pymysql.connect(host=self.host, port=self.port, user=self.user,
                                    password=self.password, db=self.db_name, charset=self.charset)
        self.cursor = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        '''
        功能描述：退出mysql时执行的操作
        参数：
        返回值：
        异常描述：
        修改记录：
        '''
        self.cursor.close()

        self.conn.close()

    def get_all(self, sql, *args, **Kwargs):
        '''
        功能描述：获取所有的数据
        参数：
        返回值：
        异常描述：
        修改记录：
        '''
        try:
            self.cursor.execute(sql, *args)
            return self.cursor.fetchall()
        except Exception as e:
            return None

    def get_one(self, sql, *args, **Kwargs):
        '''
        功能描述：获取单个数据记录
        参数：查询的条件
        返回值：
        异常描述：
        修改记录：
        '''
        try:
            self.cursor.execute(sql, *args)
            return self.cursor.fetchone()
        except Exception as e:
            return None