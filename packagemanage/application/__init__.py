from application.settings.dev import DevelopementConfig
from application.settings.pro import ProductionConfig
from flask import Flask
from flask_session import Session
from application.apps.app_v1 import blue_point
from libs.log import setup_log


# 环境配置

config = {
    'dev': DevelopementConfig,
    'prop': ProductionConfig
}


def init_app(config_name):
    '''
        项目初始化函数
    '''
    app = Flask(__name__)

    # 设置配置类

    Config = config[config_name]

    # 配置日志
    setup_log(Config)

    # 加载配置

    app.config.from_object(Config)

    # 注册蓝图

    for blue, api in blue_point:
        api.init_app(app)
        app.register_blueprint(blue)

    # 开启session功能

    Session(app)

    return app
